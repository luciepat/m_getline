#include <stdio.h>
#include <conio.h>

int main() {

	char buffer[32] = { 0 };
	char* buff = buffer;
	int size = 32;
	FILE* fp;
	fopen_s(&fp,"allo.txt", "r");
	if (!fp) {
		exit(1);
	}
	my_getline(&buff, &size, fp);
	printf("%s", buffer);
	getchar();
	return 0;
}

/*
gets string up to \n or EOF and puts it in buffer
in: pointer to buffer, pointer to buffer size, file stream (incliding STDIN)
out: size of string
*/
int my_getline(char** buffer, int* size, FILE* input_type) {
	int i = 0;
	char c = 0;
	while (i < *size) { // so we don't overflow
		c = fgetc(input_type); // get single char from stream
		if (c == '\n') {
			*(*buffer + sizeof(char)*i) = c;
			break; // we done
		}
		else if(c == EOF){ // we done
			*(*buffer + sizeof(char)*i) = 0;
			break;
		}
		*(*buffer + sizeof(char)*i) = c;
		i++;
	}
	return i;
}